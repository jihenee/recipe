<?php


namespace App\Controller;

use App\Entity\Publication;
use App\Entity\Reaction;
use App\Entity\User;
use App\Entity\UserUser;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use Psr\Log\NullLogger;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use FOS\RestBundle\Controller\Annotations as Rest;

// alias pour toutes les annotations
use Swagger\Annotations as SWG;

/**
 * Follow controller.
 * @Route("/api", name="api_")
 */
class ReactionController extends FOSRestController
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Rest\Post(
     *     path="/reaction/{publication}",
     *     name="reaction"
     *   )
     * @SWG\Response(
     *     response=200,
     *     description="Reaction posted",
     *    ),
     * @SWG\Parameter( name="Authorization", in="header", required=true, type="string", default="Bearer TOKEN", description="Authorization" ),
     * @param Request $request
     * @return \FOS\RestBundle\View\View
     */
    public function postReactionAction(Request $request)
    {
        $pub = $this->entityManager->getRepository(Publication::class)->findOneBy(
            array(
                "id" => $request->get('publication')
            ));
        $reaction = $this->entityManager->getRepository(Reaction::class)->findOneBy(
            array('user' => $pub->getUser(), 'publication' => $pub)
        );
        if (($reaction and $reaction->getAppreciate() == NULL) or !$reaction) {
            $reaction = new Reaction();
            $reaction->setUser($pub->getUser());
            $reaction->setPublication($pub);
            $reaction->setComment($request->get('comment'));
            $reaction->setAppreciate($request->get('like'));
            $this->entityManager->persist($reaction);
            $this->entityManager->flush();
            return $reaction;
        } elseif ($reaction->getAppreciate() != NULL)
            return new JsonResponse(['message' => 'Post already liked'], Response::HTTP_BAD_REQUEST);
    }
}