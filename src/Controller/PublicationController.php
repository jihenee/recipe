<?php

namespace App\Controller;

use App\Entity\Publication;
use App\Entity\UserUser;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use FOS\RestBundle\Controller\Annotations as Rest;

// alias pour toutes les annotations
use Swagger\Annotations as SWG;


/**
 * Follow controller.
 * @Route("/api", name="api_")
 */
class PublicationController extends FOSRestController
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;


    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Rest\Post(
     *     path="/publication",
     *     name="publication"
     *   )
     * @SWG\Response(
     *     response=200,
     *     description="Publication créee",
     *    ),
     * @SWG\Parameter( name="Authorization", in="header", required=true, type="string", default="Bearer TOKEN", description="Authorization" ),
     * @SWG\Parameter(
     *         name="file",
     *         in = "formData",
     *         description="Description",
     *         required=false,
     *         type="file"
     *     )
     * @SWG\Parameter(
     *         name="visibility",
     *         in="formData",
     *         description="visibility",
     *         required=false,
     *         type="number"
     *     ),
     * @Rest\FileParam(name="image", description="recipe file", nullable=false, image=true)
     * @param Request $request
     * @param ParamFetcher $paramFetcher
     * @return \FOS\RestBundle\View\View
     */
    public function postPublicationsAction(Request $request, ParamFetcher $paramFetcher)
    {
        $publication = new Publication();
        $publication->setContenu($request->get('name'));
        /** @var UploadedFile $file */
        $file = ($paramFetcher->get('image'));
        $filename = md5(uniqid()) . '.' . $file->guessClientExtension();
        $file->move(
            $this->getUploadsDir(),
            $filename
        );
        $user = $this->getUser();
        if ($request->get('visibility') == Publication::STATUS_PUBLIC || $request->get('visibility') == Publication::STATUS_PRIVATE || $request->get('visibility') == Publication::STATUS_AMIS) {
            $publication->setContenu($filename);
            $publication->setUser($user);
            $publication->setVisibility($request->get('visibility'));
            $publication->setContentPath('/uploads/' . $filename);
            $this->entityManager->persist($publication);
            $this->entityManager->flush();
            $data = $request->getUriForPath(
                $publication->getContentPath()
            );

            return $publication;
        } else {
            return new JsonResponse(['message' => 'Wong visibility'], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @Rest\Get("/publications", name="getPublication")
     * @SWG\Parameter( name="Authorization", in="header", required=true, type="string", default="Bearer TOKEN", description="Authorization" )
     * @SWG\Response(
     *     response=200,
     *     description="publications",
     *    )
     */
    public function getPublicationAction()
    {
        $user = $this->getUser();
        $publication = $this->entityManager
            ->getRepository(Publication::class)
            ->findBy(array("user" => $user));

        /* @var $publication Publication */

        if (empty($publication)) {
            return new JsonResponse(['message' => 'Publication not found'], Response::HTTP_NOT_FOUND);
        }
        return $publication;
    }

    /**
     * @Rest\Get("/publications/{followed}")
     * @SWG\Parameter( name="Authorization", in="header", required=true, type="string", default="Bearer TOKEN", description="Authorization" )
     * @SWG\Parameter(
     *         name="followed",
     *         in="formData",
     *         description="Followed id",
     *         required=true,
     *         type="number"
     *     ),
     * @SWG\Response(
     *     response=200,
     *     description="publications of followed person",
     *    )
     */
    public function getPublicationFollowedAction(Request $request = null)
    {
        $user = $this->getUser();
        $publication = $this->entityManager
            ->getRepository(Publication::class)
            ->findOneBy(array("user" => $request->get('followed')));

        /* @var $publication Publication */

        if (empty($publication)) {
            return new JsonResponse(['message' => 'Publication not found'], Response::HTTP_NOT_FOUND);
        }
        return $publication;
    }

    /**
     * @Rest\Get("/allPublicationsFollowed")
     * @SWG\Parameter( name="Authorization", in="header", required=true, type="string", default="Bearer TOKEN", description="Authorization" )
     * @SWG\Parameter(
     *         name="followed",
     *         in="formData",
     *         description="Followed id",
     *         required=true,
     *         type="number"
     *     ),
     * @SWG\Response(
     *     response=200,
     *     description="publications of followed person",
     *    )
     */
    public function getAllFollowedPublicationsAction(Request $request)
    {
        $user = $this->getUser();
        $targets = $this->entityManager->getRepository(UserUser::class)->findUserTarget($user);
        foreach ($targets as $key => $person) {
            $tabPub[$key] = $this->entityManager
                ->getRepository(Publication::class)
                ->findOneBy(array("user" => $person['userTarget']));
        }

        if (empty($tabPub)) {
            return new JsonResponse(['message' => 'Publication not found'], Response::HTTP_NOT_FOUND);
        }
        return $tabPub;
    }

    private function getUploadsDir()
    {
        return $this->getParameter('uploads_dir');
    }
}