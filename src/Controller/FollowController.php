<?php

namespace App\Controller;

use App\Entity\Publication;
use App\Entity\User;
use App\Entity\UserUser;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use Psr\Log\NullLogger;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use FOS\RestBundle\Controller\Annotations as Rest;

// alias pour toutes les annotations
use Swagger\Annotations as SWG;


/**
 * Follow controller.
 * @Route("/api", name="api_")
 */
class FollowController extends FOSRestController
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;


    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Rest\Post(
     *     path="/follow/{userId}",
     *     name="follow"
     *   )
     * @SWG\Response(
     *     response=200,
     *     description="Followed added",
     *    ),
     * @SWG\Parameter( name="Authorization", in="header", required=true, type="string", default="Bearer TOKEN", description="Authorization" ),
     * @param Request $request
     * @return \FOS\RestBundle\View\View
     */
    public function postFollowAction(Request $request)
    {
        $user = $this->getUser();

        $userFollow = $this->entityManager->getRepository(User::class)->findOneBy(
            array(
                'id' => $request->get('userId')
            )
        );
        if ($user->addFollowed($userFollow)) {
            $user->addFollowed($userFollow);
            $this->entityManager->persist($user);
            $this->entityManager->flush();
            return $user;
        } else
            return new JsonResponse(['message' => 'User already followed'], Response::HTTP_BAD_REQUEST);
    }

    /**
     * @Rest\Get("/followedByme")
     * @SWG\Parameter( name="Authorization", in="header", required=true, type="string", default="Bearer TOKEN", description="Authorization" )
     * @SWG\Response(
     *     response=200,
     *     description="persons who i follow",
     *    )
     */
    public function getUserTargetAction()
    {
        $user = $this->getUser();
        $userTarget = $this->entityManager
            ->getRepository(UserUser::class)
            ->findUserTarget($user);
        if ($userTarget == null)
            return new JsonResponse(["message" => "You don't follow anyone"], Response::HTTP_BAD_REQUEST);
        else {
            foreach ($userTarget as $key => $item) {
                $userFollow[$key] = $this->entityManager->getRepository(User::class)->findOneBy(
                    array('id' => $item['userTarget'])
                );
            }
            return $userFollow;
        }
    }

    /**
     * @Rest\Get("/followMe")
     * @SWG\Parameter( name="Authorization", in="header", required=true, type="string", default="Bearer TOKEN", description="Authorization" )
     * @SWG\Response(
     *     response=200,
     *     description="persons who followed me",
     *    )
     */
    public function getUserSourceAction()
    {
        $user = $this->getUser();
        $userSource = $this->entityManager
            ->getRepository(UserUser::class)
            ->findUserSource($user);
        if ($userSource == null)
            return new JsonResponse(['message' => 'You have not followers'], Response::HTTP_BAD_REQUEST);
        else {
            foreach ($userSource as $key => $item) {
                $userFollow[$key] = $this->entityManager->getRepository(User::class)->findOneBy(
                    array('id' => $item['userSource'])
                );
            }
            return $userFollow;
        }
    }

}