<?php
namespace App\Controller;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use Symfony\Component\HttpFoundation\Response;
use FOS\OAuthServerBundle\Model\ClientManagerInterface;
use Swagger\Annotations as SWG;
use FOS\RestBundle\Controller\Annotations as Rest;

class SecurityController extends AbstractFOSRestController
{
    private $client_manager;
    public function __construct(ClientManagerInterface $client_manager)
    {
        $this->client_manager = $client_manager;
    }
    /**
     * @Rest\Post(
     *     path="/api/login_check",
     *     name="login"
     *   )
     * @return JsonResponse
     */
    public function login(): JsonResponse
    {
        $user = $this->getUser();
        return  $this->json(array(
            'username' => $user->getUsername(),
            'roles' => $user->getRoles()
        ));
    }
}