<?php
/**
 * Created by PhpStorm.
 * User: tlili
 * Date: 11/14/17
 * Time: 3:28 PM
 */

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\OAuthServerBundle\Entity\Client as BaseClient;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table("client_scope")
 * @ORM\Entity(repositoryClass="App\Repository\Totr\ClientScopeRepository")
 */
class ClientScope
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Client",
     *     cascade={"persist"},
     *     inversedBy="clientScopes",
     * )
     * @ORM\JoinColumn(nullable=false)
     */
    protected $client;

    /**
     * @var Scope
     * @ORM\ManyToOne(
     *     targetEntity="Scope",
     *     cascade={"persist"},
     *     inversedBy="clientScopes",
     *     fetch="EAGER"
     * )
     * @ORM\JoinColumn(nullable=false)
     */
    protected $scope;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $createdAt;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param mixed $client
     * @return ClientScope
     */
    public function setClient($client)
    {
        $this->client = $client;
        return $this;
    }

    /**
     * @return Scope
     */
    public function getScope()
    {
        return $this->scope;
    }

    /**
     * @param mixed $scope
     * @return ClientScope
     */
    public function setScope($scope)
    {
        $this->scope = $scope;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}
