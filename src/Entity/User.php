<?php
namespace App\Entity;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    const STATUS_Follow= 1 ;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    public function __construct()
    {
        parent::__construct();

        $this->followed = new \Doctrine\Common\Collections\ArrayCollection();
        $this->follower = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     *@ORM\ManyToMany(targetEntity="User", mappedBy="followed", cascade={"persist"})
     * @ORM\JoinTable(name="user_user")
     *@ORM\joinColumn(nullable=true)
     */
    private $follower;

    /**
     *@ORM\ManyToMany(targetEntity="User", inversedBy="follower", cascade={"persist"})
     * @ORM\JoinTable(name="user_user")
     *@ORM\joinColumn(nullable=true)
     */
    private $followed;

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getFollower(): \Doctrine\Common\Collections\ArrayCollection
    {
        return $this->follower;
    }

    /**
     * @param \Doctrine\Common\Collections\ArrayCollection $follower
     */
    public function setFollower(\Doctrine\Common\Collections\ArrayCollection $follower): void
    {
        $this->follower = $follower;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getFollowed(): \Doctrine\Common\Collections\ArrayCollection
    {
        return $this->followed;
    }

    /**
     * @param \Doctrine\Common\Collections\ArrayCollection $followed
     */
    public function setFollowed(\Doctrine\Common\Collections\ArrayCollection $followed): void
    {
        $this->followed = $followed;
    }


    public function addFollowed(User $user): ?self
    {
        if (!$this->followed->contains($user)) {
            $this->followed[] = $user;
            return $this;
        }
        else
            return null;
    }

    public function removeFollowed(User $user): self
    {
        if ($this->followed->contains($user)) {
            $this->followed->removeElement($user);
        }
        return $this;
    }

}