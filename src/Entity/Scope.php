<?php
/**
 * Created by PhpStorm.
 * User: tlili
 * Date: 11/14/17
 * Time: 3:28 PM
 */

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table("scope")
 * @ORM\Entity(repositoryClass="App\Repository\Security\ScopeRepository")
 */
class Scope
{

    const MESSAGES = array(
        'message.read',
        'message.write',
    );

    const LOCATION = array(
        'location.read',
        'location.write',
    );

    const PROVISIONING = array(
        'provisioning.read',
        'provisioning.write'
    );

    const PROCESSES = array(
        'process.read',
        'process.write',
    );

    const STATISTICS = array(
        'statistics.read',
        'statistics.write',
    );

    const SUPERADMIN = array(
        'superadmin.read',
        'superadmin.write',
    );


    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(name="name", type="string", length=255)
     */
    protected $name;

    /**
     * @var string
     * @ORM\Column(name="description", type="text",nullable=true)
     */
    protected $description;

    /**
     * @var ArrayCollection|ClientScope[]
     * @ORM\OneToMany(targetEntity="ClientScope", mappedBy="scope")
     */
    protected $clientScopes;

    /**
     * @var array
     * @ORM\Column(name="functions", type="array", nullable=true)
     */
    protected $functions;

    /**
     * @ORM\Column(type="datetime")
     * @var
     */
    private $createdAt;

    public function __construct()
    {
        $this->functions = array();
        $this->clientScopes = new ArrayCollection();
        $this->createdAt = new \DateTime();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    public function addFunction($function)
    {
        if (!in_array($function, $this->functions, true)) {
            $this->functions[] = $function;
        }

        return $this;
    }

    /**
     * @return array
     */
    public function getFunctions()
    {

        return $this->functions;
    }

    /**
     * @param array $functions
     */
    public function setFunctions($functions)
    {
        $this->functions = $functions;
    }

    public function hasFunction($function)
    {
        return in_array($function, $this->getFunctions(), true);
    }


    public function removeFunction($function)
    {
        if (false !== $key = array_search($function, $this->functions, true)) {
            unset($this->functions[$key]);
            $this->functions = array_values($this->functions);
        }

        return $this;
    }

    /**
     * @return Client[]|ArrayCollection
     */
    public function getClientScopes()
    {
        return $this->clientScopes;
    }

    /**
     * @param Client[]|ArrayCollection $clientScopes
     * @return Scope
     */
    public function setClientScopes($clientScopes)
    {
        $this->clientScopes = $clientScopes;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }
}
