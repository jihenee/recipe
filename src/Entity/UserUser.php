<?php


namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserUserRepository")
 * @ORM\Table(name="user_user")
 */
class UserUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     */
    protected $userSource;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     */
    protected $userTarget;

    /**
     * @return mixed
     */
    public function getUserSource()
    {
        return $this->userSource;
    }

    /**
     * @param mixed $userSource
     */
    public function setUserSource($userSource): void
    {
        $this->userSource = $userSource;
    }

    /**
     * @return mixed
     */
    public function getUserTarget()
    {
        return $this->userTarget;
    }

    /**
     * @param mixed $userTarget
     */
    public function setUserTarget($userTarget): void
    {
        $this->userTarget = $userTarget;
    }
}