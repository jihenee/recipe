<?php


namespace App\Repository;


use App\Entity\User;
use App\Entity\UserUser;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

class UserUserRepository extends EntityRepository
{
    public function findUserTarget(User $user)
    {
        return
            $this
                ->createQueryBuilder('uu')
                ->select('uu.userTarget')
                -> where('uu.userSource = :user')
                ->setParameter('user', $user->getId() )
                ->getQuery()
                ->getResult();
    }

    public function findUserSource(User $user)
    {
        return
            $this
                ->createQueryBuilder('uu')
                ->select('uu.userSource')
                -> where('uu.userTarget = :user')
                ->setParameter('user', $user->getId() )
                ->getQuery()
                ->getResult();
    }
}